var app = angular.module('myApp', []);
app.controller('myCtrl', ['$scope', '$http',
                          function($scope, $http) {
	var that = $scope;
	that.jobs = {};
	that.jobTypeFilter = {};
	
	$scope.$watch("jobTypeFilter", function() {
       that.jobs = {};
    }, true);
	
	
	that.postData = function(){
		$http({
			headers: { 
				'Accept': 'application/json',
				'Content-Type': 'application/json' 
			},
		  'method': 'POST',
		  'url': 'addJob.html',
		  'data': {"title": that.title},
		  'dataType': 'json'
		}).success(function(response) {
			//that.mydata = response;
			that.getData();
		}).error( function(response) {
			console.log(response);
		});
		return;
	};
	
	that.getData = function(){
		$http({
			headers: { 
				'Accept': 'application/json',
				'Content-Type': 'application/json' 
			},
		  'method': 'GET',
		  'url': 'getJobList.html',
		  'dataType': 'json'
		}).success(function(response) {
			that.mydata = response;
		}).error( function(response) {
			console.log(response);
		});
		return;
	};
 
	that.getData();
	
	that.upData = function(updateJobId, updateJobStatus){
		$http({
			headers: { 
				'Accept': 'application/json',
				'Content-Type': 'application/json' 
			},
		  'method': 'POST',
		  'url': 'updateJob.html',
		  'data': {"id": updateJobId, "status": updateJobStatus},
		  'dataType': 'json'
		}).success(function(response) {
			that.getData();
		}).error( function(response) {
			console.log(response);
		});
		return;
	};
}]);
