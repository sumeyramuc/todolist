<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript"
	src="<c:url value="resources/js/jquery-3.1.0.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="resources/js/angular.min.js"/>"></script>
<link rel="stylesheet"
	href="<c:url value="resources/css/bootstrap.min.css"/>">
<script type="text/javascript"
	src="<c:url value="resources/js/bootstrap.min.js"/>"></script>

<html>
<head>
<title>Spring MVC Tutorial</title>
<style type="text/css">
body {
	background-image: url('resources/image/bg.png');
}
</style>
</head>
<body ng-app="myApp">
	${message}

	<br>
	<br>
	<section ng-controller="myCtrl">
		<form name="form1" novalidate class="css-form">
			<div
				ng-class="{'has-warning':form1.todoTitle.$error.required||form1.todoTitle.$error.minlength, 
           		'has-error': form1.todoTitle.$error.maxlength}"
				class="form-group has-feedback">

				<label class="control-label" for="todoTitle">Task Title: </label> <input
					name="todoTitle" type="text" class="form-control" id="todoTitle"
					ng-model="title" ng-minlength="1" ng-maxlength="45" required
					ng-required="true"> <span
					class="glyphicon glyphicon-warning-sign form-control-feedback"
					ng-show="form1.todoTitle.$error.required||form1.todoTitle.$error.minlength"></span>
				<label class="control-label" for="todoTitle"
					ng-show="form1.todoTitle.$error.required||form1.todoTitle.$error.minlength">This
					field cannot be empty</label> <span
					class="glyphicon glyphicon-remove form-control-feedback"
					ng-show="form1.todoTitle.$error.maxlength"></span> <label
					class="control-label" for="todoTitle"
					ng-show="form1.todoTitle.$error.maxlength">Character limit
					is 45</label>

				<%--<div class="form-group">
				    <%--<label for="todoStatus">Todo Status:</label>
		  			<input name="todoStatus" type="text" class="form-control" id="todoStatus" ng-model="status"
		  			ng-minlength="1" ng-maxlength="45" required ng-required="true">
		            <span style="color:orange" ng-show="form1.todoStatus.$error.required||form1.todoStatus.$error.minlength">This field cannot be empty.</span>
		            <span style="color:red" ng-show="form1.todoStatus.$error.maxlength">Character limit is 45.</span>
				</div> --%>

				<div class="row">
					<div class="col-sm-12">
						<button type="button" class="btn btn-success top-buffer"
							ng-click="postData()" ng-disabled="form1.$invalid">CREATE</button>
					</div>
				</div>

			</div>


		</form>
		<form name="form2" novalidate class="css-form">
			<br>

			<h4>Search in table:</h4>
			<p>
				<input type="text" class="form-control" ng-model="filterField">
			</p>

			<h3>TODO List:</h3>

			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th width="10%">Choose</th>
						<th width="30%">Title</th>
						<th width="30%">Status <br> <input type="checkbox"
							ng-model="jobTypeFilter.filter" ng-true-value=""
							ng-false-value="" /> <label>ALL</label> <input type="checkbox"
							ng-model="jobTypeFilter.filter" ng-true-value="'DONE'"
							ng-false-value="" style="margin-left: 5%" /> <label
							class="success">DONE</label> <input type="checkbox"
							ng-model="jobTypeFilter.filter" ng-true-value="'UNDONE'"
							ng-false-value="" style="margin-left: 5%" /> <label>UNDONE</label>

							<input type="checkbox" ng-model="jobTypeFilter.filter"
							ng-true-value="'DELETED'" ng-false-value=""
							style="margin-left: 5%" /> <label>DELETED</label>
						</th>
						<th width="30%">Log Date</th>
					</tr>
				</thead>
				<tbody>
					<!-- 		    ng-repeat ile array dönüp table oluştur -->
					<tr
						ng-repeat="data in mydata |filter:filterField | filter:{status: jobTypeFilter.filter} : true |filter:search:strict"
						ng-class="{'success' : data.status=='DONE',
			      'active' : data.status=='UNDONE', 'danger' : data.status=='DELETED'}">

						<td width="10%"><input type="checkbox" class="form-control"
							ng-model="jobs.job" ng-true-value="{{data}}" ng-false-value=""
							style="width: 50% !important;"></td>
						<td style="vertical-align: middle !important; width: 30%">
							{{data.title}}</td>
						<td style="vertical-align: middle !important; width: 30%">
							{{data.status}}</td>
						<td style="vertical-align: middle !important; width: 30%">
							{{data.createDate | date:'yyyy/MM/dd HH:mm:ss'}}</td>
					</tr>
				</tbody>
			</table>
			<input type="button" class="btn btn-danger"
				ng-disabled="jobs.job.status=='DELETED'||jobs.job.status==undefined"
				value="DELETE" ng-click="upData(jobs.job.id, 'DELETED')"> <input
				type="button" class="btn btn-success"
				ng-disabled="jobs.job.status=='DONE'||jobs.job.status=='DELETED'||jobs.job.status==undefined"
				value="DONE" ng-click="upData(jobs.job.id, 'DONE')">

			<!-- 		Coming from Func: {{fullName()}} -->
		</form>
	</section>
	<!-- 	<section ng-controller="mySecondCtrl"> -->
	<!-- 		Coming from Func: {{fullName()}} -->
	<!-- 	</section> -->
</body>

<link rel="stylesheet"
	href="<c:url value="resources/usr_css/usrstyle.css"/>">
<script type="text/javascript"
	src="<c:url value="resources/usr_js/examplejs.js"/>"></script>

</html>