package com.example.project.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.project.dao.JobDAO;
import com.example.project.model.Job;


@Service
public class JobServiceImpl implements JobService {
	
	private JobDAO jobDAO;

	public void setJobDAO(JobDAO jobDAO) {
		this.jobDAO = jobDAO;
	}

	@Override
	@Transactional
	public void addJob(Job p) {
		this.jobDAO.addJob(p);
	}

	@Override
	@Transactional
	public void updateJob(Job p) {
		this.jobDAO.updateJob(p);
	}

	@Override
	@Transactional
	public List<Job> listJobs() {
		return this.jobDAO.listJobs();
	}

	@Override
	@Transactional
	public Job getJobById(int id) {
		return this.jobDAO.getJobById(id);
	}

	@Override
	@Transactional
	public void removeJob(int id) {
		this.jobDAO.removeJob(id);
	}

}
