package com.example.project.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.example.project.model.Job;
import com.example.project.model.JobTypes;
import com.example.project.service.JobService;

@RestController
@EnableWebMvc
public class MainController {

	@Autowired
	private JobService jobService;
	
//	@Autowired(required=true)
//	@Qualifier(value="jobService")
//	public void setJobService(JobService ps){
//		this.jobService = ps;
//	}
	
	@RequestMapping("/welcome")
	public ModelAndView helloWorld() {
		String message = "<br><div id='firstDiv' style='text-align:center;'>"
				+ "<h3>********** Hello World, Spring MVC Tutorial</h3>This message is coming from back-end **********</div><br><br>";
		return new ModelAndView("welcome", "message", message);
	}

	@RequestMapping(value = "/addJob", method = RequestMethod.POST)
	public void addJob(@RequestBody Job request, Model model) {
		request.setStatus(JobTypes.UNDONE);
		this.jobService.addJob(request);
	}
	
	@RequestMapping(value = "/getJobList", method = RequestMethod.GET)
	public @ResponseBody List<Job> getJobList() {
		List<Job> jobList = this.jobService.listJobs();
		return jobList;
	}
	@RequestMapping(value = "/updateJob", method = RequestMethod.POST)
	public void updateJob(@RequestBody Job request, Model model) {
		Job updateJob = this.jobService.getJobById(request.getId());
		updateJob.setStatus(request.getStatus());
		this.jobService.updateJob(updateJob);
	}
}