package com.example.project.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="yapilacaklar")
public class Job {
	
	@Id
//	@Column(name="id")
	//@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@NotEmpty
    @Column(name="yapilacak", nullable=false)
	@Length(max = 45, message = "The title field must be less than 45 characters")
	private String title;
	
	@NotEmpty
    @Column(name="durum", nullable=false)
	@Length(max = 45, message = "The status field must be less than 45 characters")
	private String status;
	
	@Column(name="timestamp", nullable=false)
	@Type(type="timestamp")
	private Date createDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
