package com.example.project.model;

public class JobTypes {
	public static final String DONE = "DONE";
	public static final String UNDONE = "UNDONE";
	public static final String DELETED = "DELETED";
}
