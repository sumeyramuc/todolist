package com.example.project.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.example.project.model.Job;


@Repository
public class JobDAOImpl implements JobDAO {
	

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addJob(Job p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);
		System.out.println("Job saved successfully, Job Details="+p);
	}

	@Override
	public void updateJob(Job p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		System.out.println("Job updated successfully, Job Details="+p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Job> listJobs() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Job> jobsList = session.createQuery("from Job").list();
		for(Job p : jobsList){
			System.out.println("Job List::"+p);
		}
		return jobsList;
	}

	@Override
	public Job getJobById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Job p = (Job) session.load(Job.class, new Integer(id));
		System.out.println("Job loaded successfully, Job details="+p);
		return p;
	}

	@Override
	public void removeJob(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Job p = (Job) session.load(Job.class, new Integer(id));
		if(null != p){
			session.delete(p);
		}
		System.out.println("Job deleted successfully, job details="+p);
	}

}